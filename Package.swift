// swift-tools-version:4.0

import PackageDescription

let package = Package(
    name: "keccak256",
    products: [
       .library(name:"keccak256", targets: ["keccak256"])
    ],
    targets: [
       .target(name: "keccak256", path: ".", sources: ["Sources"])
    ]
)
